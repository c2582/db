#!/bin/bash
openssl rand -base64 756 > mongodb.key
chmod 600 mongodb.key

docker-compose up -d
sleep 15
docker-compose exec configsvr01 sh -c "mongo < /scripts/init-configserver.js"
docker-compose exec shard01-a sh -c "mongo < /scripts/init-shard01.js"
docker-compose exec shard02-a sh -c "mongo < /scripts/init-shard02.js"
docker-compose exec shard03-a sh -c "mongo < /scripts/init-shard03.js"
sleep 15
docker-compose exec router01 sh -c "mongo < /scripts/init-router.js"
sleep 5
docker-compose exec router01 sh -c "mongo < /scripts/initDB2.js"