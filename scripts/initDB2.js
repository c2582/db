sh.enableSharding("covidmap")
db.adminCommand( { shardCollection: "covidmap.PATIENT", key: {"_id":1} } )
db.adminCommand( { shardCollection: "covidmap.DISTRICT", key: { "_id": 1} } )
db.adminCommand( { shardCollection: "covidmap.POINT", key: { _id: 1 } } )
db.adminCommand( { shardCollection: "covidmap.REGION", key: { _id: 1 } } )
